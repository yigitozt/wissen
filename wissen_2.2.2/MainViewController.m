//
//  MainViewController.m
//  wissen_2.2.2
//
//  Created by Yigit Ozturk on 3.11.2013.
//  Copyright (c) 2013 wissen. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    // self.view.backgroundColor = [UIColor yellowColor];
    
    UIColor *color = self.view.backgroundColor;
    NSLog(@"%@", color);
    
    
    UILabel *labelOnTheScreen = (UILabel *)[self.view viewWithTag:12];
    NSLog(@"Text on the screen is %@", labelOnTheScreen.text);
    labelOnTheScreen.text = @"Wissen Happy Weekends";
    
    
    CGRect screenSize = self.view.bounds;
    NSLog(@"%f - %f - %f - %f", screenSize.origin.x,
                                screenSize.origin.y,
                                screenSize.size.width,
                                screenSize.size.height);
    //yari boyutlarini verir
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}



@end

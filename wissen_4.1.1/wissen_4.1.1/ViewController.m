//
//  ViewController.m
//  wissen_4.1.1
//
//  Created by Yigit Ozturk on 23.11.2013.
//  Copyright (c) 2013 yigitozturk. All rights reserved.
//

#import "ViewController.h"
#import "SecondViewController.h"

@interface ViewController ()
{
    NSDictionary *dict;
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    
     dict = @{@"alpha":@[@"Emre", @"Tirnovali"],
                           @"beta":@[@"Ersun", @"Tirnovali"],
                           @"gama":@[@"Halit", @"Tirnovali"],
                           };
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    /*
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //bu tanimlamayi nerde tanimlarsak tanimlayalim ayni adresi verir.
    
    [defaults setObject:@"Hello World" forKey:@"stringvalue"];
    
    */
    
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
 
    NSLog(@"Source Controller = %@", [segue sourceViewController]);
    NSLog(@"Destination Controller = %@", [segue destinationViewController]);
    NSLog(@"Segue Identifier = %@", [segue identifier]);
    
    /*
    if([[segue identifier]isEqualToString:@"secondViewSegue"])
    {
        ((SecondViewController*)[segue destinationViewController]).view.backgroundColor = [UIColor greenColor];
    }
     */
    //source yola cikilan scenenin kendisi
    //identifier gidilenin bizim verdigimiz adi
    //destination gidilecek olan scene
    
    
    if([[segue identifier]isEqualToString:@"secondViewSegue"])
    {
        ((SecondViewController*)[segue destinationViewController]).textFieldString = self.wTextField.text;
    }

    
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:dict forKey:@"dictionaryValue"];
    
    [defaults setObject:self.wTextField.text forKey:@"stringValue"];

    //gidecek ekran yani secondVC ve bunun icin olusturdugumuz SecondViewController sinifinda olusturdugumuz NSString e text fielddaki stringi esitliyoruz ve daha sonra SecondViewController.m de bu NSString i label.text e esiliyoruz.
    
    
    
}
//bu metod bir sayfadan diger safaya gecerken calistirilan bir metod
//storyboard kullanirsak calisiyor
//gecis ayarinda identifier olarak bir string giriyoruz "secondViewSegue" girdik


- (IBAction)unwindToMain:(UIStoryboardSegue*)unwindSegue
{
    
}
//unWinding methodu yani geri gitme
//bu metod hangi viewcontrollera gidilecek ise oraya acilir ve butonu exitebiraktigimizde bu metodlar gorunur





@end

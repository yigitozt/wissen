//
//  SecondViewController.h
//  wissen_4.1.1
//
//  Created by Yigit Ozturk on 23.11.2013.
//  Copyright (c) 2013 yigitozturk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *wLabel;

@property(nonatomic,strong)NSString * textFieldString;


@end

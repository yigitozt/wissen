//
//  ForthViewController.m
//  wissen_4.1.1
//
//  Created by Yigit Ozturk on 23.11.2013.
//  Copyright (c) 2013 yigitozturk. All rights reserved.
//

#import "ForthViewController.h"

@interface ForthViewController ()

@end

@implementation ForthViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.defaultsLabel.text = [defaults objectForKey:@"stringValue"];
    //stringValue olarak kaydettigimiz(viewController) veriyi aliyoruz
    
    self.middleLabel.text = @"hello world world hello world hello world";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

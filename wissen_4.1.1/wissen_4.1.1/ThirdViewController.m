//
//  ThirdViewController.m
//  wissen_4.1.1
//
//  Created by Yigit Ozturk on 23.11.2013.
//  Copyright (c) 2013 yigitozturk. All rights reserved.
//

#import "ThirdViewController.h"

@interface ThirdViewController ()

@end

@implementation ThirdViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
	
    
    UIView *cView = [UIView new];
    UIView *tView = [UIView new];
    
    [cView setTag:101];
    
    cView.backgroundColor = [UIColor redColor];
    tView.backgroundColor = [UIColor blueColor];
    [self.view addSubview:cView];//kesinlikle once eklenir
    [self.view addSubview:tView];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-50-[cView(40)][tView]-50-|"
                                                                     options:0 metrics:nil
                                                                       views:NSDictionaryOfVariableBindings(cView, tView)]];
    //NSDictionaryOfVariableBindings nesneleri string olarak algilatmamizi saglar
    
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-100-[cView]-100-|"
                                                                      options:0 metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(cView, tView)]];
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-100-[tView]-100-|"
                                                                      options:0 metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(tView)]];
    
    
    [cView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [tView setTranslatesAutoresizingMaskIntoConstraints:NO];
    //eklenen view in autoresizing mask i kapatilir
    
}





- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

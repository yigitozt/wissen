//
//  main.m
//  wissen_2.1.1
//
//  Created by Yigit Ozturk on 2.11.2013.
//  Copyright (c) 2013 wissen. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

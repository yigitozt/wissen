//
//  NSMutableString+Additional.m
//  wissen_2.1.1
//
//  Created by Yigit Ozturk on 2.11.2013.
//  Copyright (c) 2013 wissen. All rights reserved.
//

#import "NSMutableString+Additional.h"

//mutable sonradan degistirilebilir
//mutable lar daha fazla degistirmeye olanak saglarlar

@implementation NSMutableString (Additional)

-(void)getBiggerText
{
    [self uppercaseString];
}

@end

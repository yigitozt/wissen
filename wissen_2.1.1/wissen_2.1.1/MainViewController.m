//
//  MainViewController.m
//  wissen_2.1.1
//
//  Created by Yigit Ozturk on 2.11.2013.
//  Copyright (c) 2013 wissen. All rights reserved.
//

#import "MainViewController.h"
#import "NSString+Additional.h" //kategoriyi getirdik
#import "NSMutableString+Additional.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    NSMutableString *mString = [[NSMutableString alloc]initWithString:@"big"];
    [mString appendString:@"name"];
    //mStringi direk olarak etkileyebiliyoruz mutableString oldugundan
    // yani mString = @"" demeden
    
    NSInteger tempInt = [mString length];// stringin uzunlugu
    
    NSLog(@"%@",mString);
    [mString length];
    char m = [mString characterAtIndex:0];
    // string bir karakter dizisidir
    
    // bugun hava ne kadar da guzel o kadar guzel olmasa da olurdu.
    
    NSMutableString * longMutableString = [[NSMutableString alloc]initWithString:@"bugun hava ne kadar da guzel o kadar guzel olmasa da olurdu."];
    //mutable oldugu icin direk tanimliyoruz bu sekilde alloc ve init ile
    
    NSRange prettyWordsRange = [longMutableString rangeOfString:@"guzel"];
    //cumledeki guzel kelimesinin baslangic index ini ve uzunlugunu aldik
    
    [longMutableString deleteCharactersInRange:prettyWordsRange];
    //cumledeki belirledigimiz aralik bilgisindeki stringi sildik
    
   NSString * replacedStr = [longMutableString stringByReplacingOccurrencesOfString:@"guzel" withString:@"cirkin"];
    //string icerisinde bir stringin bir baskasi ile degistirmek icin kullanilan metod
    
    
    
   NSString * removedStr = [longMutableString stringByReplacingOccurrencesOfString:@"guzel" withString:@""];
    //string icerisindeki bir kelime harf yada boslugu istegidimiz bir string yada bosluk ile degistirme
    
    
    
    /*
     
     %c -- char
     %i -- int
     %f -- float and double
     
     
     */
    
    
    
    
    NSLog(@"%@", longMutableString);
    
    
    
    NSRange range = NSMakeRange(0, 2);
    [mString deleteCharactersInRange:range];
    NSLog(@"deleted string = %@",mString);
    NSLog(@"%c",m);
    
   // NSString * str = [NSString stringWithFormat:@"%@ %@, @"big", %@"@"name"];
    
    
    
    
    NSString * tempStr = nil;
    NSArray * array = [[NSArray alloc]initWithObjects:@"Ersun",@"Umut",@"Yigit", nil];
    //ilk array olusturma versiyonu atasi
    NSArray * arrayOne = [NSArray arrayWithObjects:@"Ersun",@"Umut",@"Yigit", nil];
    //array olusturmanin daha gelismis versiyonu
    NSArray * arrayTwo = @[@"Ersun",@"Umut",@"Yigit"];
    //array olusturmanin shortHand yontemi, crash problemi olabilir ancak
    //bu gosterimde nil nesneler crash e sebep olur mesela tempStr
    
    
    
    for (NSString *s in array)
    {
        NSLog(@"%@", s);
    }
    
    //hizli bir sekilde array nesnelerinin icini sira ile dondurup bir islem yapmak istedigimizde basvurdugumuz yapidir.
    //Anlami : array nesnesi icindeki her bir string s degeri.
    
    
    NSString *studentName = [array objectAtIndex:1];//old way
    NSString *teacherName = array[2];//shorthand
    NSLog(@"%@", studentName);
    NSLog(@"%@", teacherName);
    
    //arrayde istenilen elemana erisme
    
    
    
    NSMutableArray *mutableArray = [[NSMutableArray alloc]init];
    [mutableArray addObject:@"Afsin"];
    [mutableArray addObject:@"Burge"];
    [mutableArray addObject:@"Yunus"];
    [mutableArray addObject:@"Ejber"];// siradan atama
    [mutableArray addObject:@"Yunus"];
    [mutableArray insertObject:@"Halit" atIndex:2];//index e atama
    //[mutableArray removeObjectAtIndex:2];//indexten silme
    [mutableArray removeObject:@"Yunus"];//butun yunus lari siler
    //[mutableArray removeAllObjects];//herseyi siler arraydan
    
    
    for (NSString *s in mutableArray)
    {
        NSLog(@"%@", s);
    }

    
    
    //----------
    
    NSMutableArray *tempArray = [array mutableCopy];
    [tempArray addObject:@"Emre"];
    
    for (NSString *s in tempArray)
    {
        NSLog(@"%@", s);
    }
    // burada array i alip tempArraye atti ve mutable degilse bile mutable yapar
    
    
    
    NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:@"Sefiller",@"Kitap",
                                                                    @"Victor Hugo",@"Yazar",
                                                                    nil];
    // 2li olarak degerler atanir, 2. degerler unique olmali.
    
    //dict = @{ k1 : o1, k2 : o2, k3 : o3} shorthand
    
    
    
    
    NSDictionary *tempDict = @{@"name": @"Yigit", @"Surname": @"Ozturk", @"Age" : @24};
    //24 un basina @ koyuyoruz cunku nsnumber a donusmeli(class), yoksa hata verir.
    
    
    NSString *personName = tempDict[@"name"];

    
    
   NSString * bookName = [dict objectForKey:@"Kitap"];
    
   NSLog(@"Book name is = %@", bookName);
    
    
    
    
    ///Mutable dictionary
    
    NSMutableDictionary * mutableDictionary = [[NSMutableDictionary alloc]init];
    [mutableDictionary setObject:@"Burge" forKey:@"name"];
    [mutableDictionary setObject:@"Ahmet" forKey:@"name"];
    //ayni keyden set olursa bir oncekini  override eder siler ve yerine gecer
    
    [mutableDictionary removeObjectForKey:@"name"];
    //name keyindeki degeri siler.
    NSMutableDictionary * newFromOld = [tempDict mutableCopy];//unused cunku name keyindekini sildik
    //mutable olmayan tempDict nesnesini kopyalayip yeni mutable nesneye atadi.Yeni nweFromOld nesnesi mutabledir.
    
    NSString * ttempStr = mutableDictionary[@"name"];
    NSLog(@"%@", ttempStr);// ekranda Ahmet yazacaktir.
    
    ///ODEV////////
    /*
     array icine name ve surname key leri kullanarak dictionary ile ve her ikiside shorthand olacak.
     */
    
    
    
    
    
    
    
    /*
     NSString - OK
     NSMutableString - OK
     NSArray - enumarable -OK
     NSMutableArray - enumarable
     NSDictionary - enumarable
     NSMutableDictionary - enumarable
     NSNumber
     --------
     loops
     if-else
     switch
     
    */
    
    
    
    
    /////////////////////
   /////////////PATTERN BULMA CALISMASI////////////////

    
    NSString * patternString = @"ooobbaskcccttttt";
    bool firstTime = 0;
    char firstChar,secondChar;
    int patternCount = 0;
    
    for(int i=0;i<[patternString length] - 1;i++)
    {
        firstChar = [patternString characterAtIndex:i];
        secondChar = [patternString characterAtIndex:i+1];
        
        if(firstChar == secondChar)
        {
            if(!firstTime)
            {
                patternCount += 1;
                firstTime = true;
            }
        }
        else
        {
            firstTime = false;
        }
    }
    
    NSLog(@"%i",patternCount);
    
///////////////////END OF PATTERN BULMA CALISMASI////////////////
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

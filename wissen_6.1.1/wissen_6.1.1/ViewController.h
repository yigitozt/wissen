//
//  ViewController.h
//  wissen_6.1.1
//
//  Created by Yigit Ozturk on 7.12.2013.
//  Copyright (c) 2013 yigitozturk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, strong) UIDatePicker *datePicker;

@property (strong, nonatomic) IBOutlet UIPickerView *pickerView;

@end

//
//  ViewController.m
//  wissen_6.1.1
//
//  Created by Yigit Ozturk on 7.12.2013.
//  Copyright (c) 2013 yigitozturk. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    NSDictionary *states;
    NSArray *stateNames;
    NSArray * provinceArray;
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    stateNames = @[@"istanbul", @"izmir", @"ankara", @"bursa"];
    
    states = @{@"istanbul" : @[@"bagcilar", @"gungoren",@"pendik", @"zeytinburnu"],
               @"izmir" : @[@"cigli", @"menderes",@"karsiyaka", @"buca"],
               @"ankara" : @[@"cankaya", @"cayyolu",@"beypazari", @"kizilay",@"yenimahalle"],
               @"bursa" : @[@"inegol", @"orhangazi",@"osmangazi", @"kemalpasa",@"kestel"]};
    
    self.pickerView.delegate = self;
    
    
     /*
    self.datePicker = [UIDatePicker new];
    self.datePicker.datePickerMode = UIDatePickerModeCountDownTimer;
    [self.view addSubview:self.datePicker];
    
    
    [self.datePicker addTarget:self action:@selector(dateChanged) forControlEvents:UIControlEventValueChanged];
    
    
    NSTimeInterval removedTime = 2 * 60;
    
    [self.datePicker setCountDownDuration:removedTime];
    
    
    
    
   
    NSDate *date = [NSDate date];
    //tarih ile etkilesme, tarihi formatlama
    
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSWeekdayCalendarUnit|NSWeekCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit|NSTimeZoneCalendarUnit fromDate:date];
    //hepsi or ile eklenebiliyor, eklenmedigi zaman hatali sonuc verebilir
    
   // [comps setYear:[comps year] + 1];//takvim yilini bir sene ileri aldik
    [comps setMonth:[comps month]];
    
    NSDate *tDateMonth = [calendar dateFromComponents:comps];
    NSLog(@"%@", tDateMonth);
    
    
    
    NSDateFormatter * formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"dd/MMMM/YYYY"];
    NSString *dateString = [formatter stringFromDate:tDateMonth];
    NSLog(@"String from date %@", dateString);
    
    ////LOGS////
    NSLog(@"%@", date);
    */
    
    
}



-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSInteger counter;
    
    if(component == 0)
    {
        counter = states.count;
    }
    else
    {
        counter = provinceArray.count;
    }
    
    return counter;
}


-(NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if(component == 0)
    {
        return [stateNames objectAtIndex:row];
    }
    else
    {
        return [provinceArray objectAtIndex:row];
    }
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(component== 0)
    {
       provinceArray = [states objectForKey:[stateNames objectAtIndex:row]];
    }
    
    [self.pickerView reloadComponent:1];
    
}



- (void)dateChanged
{
    NSDate *currentDate = self.datePicker.date;
    NSLog(@"%@", currentDate);
}











@end

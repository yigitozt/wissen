//
//  AppDelegate.h
//  wissen_6.1.1
//
//  Created by Yigit Ozturk on 7.12.2013.
//  Copyright (c) 2013 yigitozturk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

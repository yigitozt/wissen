//
//  ViewController.h
//  Homework_4.1
//
//  Created by Yigit Ozturk on 24.11.2013.
//  Copyright (c) 2013 yigitozturk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController


@property (strong, nonatomic) IBOutlet UIView *mainView;


@end

//
//  ViewController.m
//  wissen_5.1.2
//
//  Created by Yigit Ozturk on 30.11.2013.
//  Copyright (c) 2013 yigitozturk. All rights reserved.
//

#import "ViewController.h"
#import <AFNetworking/AFNetworking.h>
#import <AFNetworking/UIImageView+AFNetworking.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    NSURL *url = [NSURL URLWithString:@"http://i.imgur.com/Fx9DuCR.png"];
    
    [self.imageView setImageWithURL:url placeholderImage:[UIImage imageNamed:@""]];
    //placeholder localdeki resim. yani urlden yuklenmeden once gozukecek olan resim
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:@"http://example.com/resources.json" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        //buralarda yine √performSelectorOnMainThread metodu kullanmamiz lazim on yuze ekleme yapabilmek icin
        //yada diger yontem C fonk
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        
    }];
    
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

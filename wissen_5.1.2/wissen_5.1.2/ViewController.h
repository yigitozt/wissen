//
//  ViewController.h
//  wissen_5.1.2
//
//  Created by Yigit Ozturk on 30.11.2013.
//  Copyright (c) 2013 yigitozturk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *imageView;


@end

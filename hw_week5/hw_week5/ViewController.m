//
//  ViewController.m
//  hw_week5
//
//  Created by Yigit Ozturk on 1.12.2013.
//  Copyright (c) 2013 yigitozturk. All rights reserved.
//

#import "ViewController.h"
#import "QuartzCore/QuartzCore.h"


@interface ViewController ()
{
    bool isRegistered
        , isMenuOpened
        , isThisMainMenu
        , proceedNow
        , stillTouching
        , mainXPos;
    
    float fingerYPositionStart
        , fingerYPositionMoved
        , fingerYPositionDif
        , getYPos
        , registerYPos;
    
    UITextField *userNameTF;
    UITextField *userPasswordTF;
    UITextField *userPasswordCorrectTF;
    
    NSString *userName;
    NSString *userPassword;
    NSString *userPasswordCorrect;
    
}

@end



@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    getYPos = self.viewMain.center.y;

    [self.view bringSubviewToFront:self.viewRegister];

    
    
    
    //update function for the animations
    [NSTimer scheduledTimerWithTimeInterval:0.01 target:self
                                   selector:@selector(updateFunc) userInfo:nil repeats:YES];
    

   
    [self makeButtonsAlive];
    
}



- (void)makeButtonsAlive
{
    
    userNameTF = [[UITextField alloc]init];
    userPasswordTF = [[UITextField alloc]init];
    userPasswordCorrectTF = [[UITextField alloc]init];
    
    [userNameTF setBorderStyle:UITextBorderStyleRoundedRect];
    [userPasswordTF setBorderStyle:UITextBorderStyleRoundedRect];
    [userPasswordCorrectTF setBorderStyle:UITextBorderStyleRoundedRect];
    
    [userPasswordTF setKeyboardType:UIKeyboardTypeNumberPad];
    [userPasswordCorrectTF setKeyboardType:UIKeyboardTypeNumberPad];
    
    [userNameTF setTag:10];
    [userPasswordTF setTag:11];
    [userPasswordCorrectTF setTag:12];
    
    [self.viewRegister addSubview:userNameTF];
    [self.viewRegister addSubview:userPasswordTF];
    [self.viewRegister addSubview:userPasswordCorrectTF];
    
    
    
    
    
    
    

    
    [self.viewRegister addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-100-[userNameTF][userPasswordTF][userPasswordCorrectTF]-350-|"
                                                                      options:0 metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(userNameTF, userPasswordTF, userPasswordCorrectTF)]];
    
    [self.viewRegister addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-100-[userNameTF]-100-|"
                                                                              options:0 metrics:nil
                                                                                views:NSDictionaryOfVariableBindings(userNameTF)]];
    
    [self.viewRegister addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-100-[userPasswordTF]-100-|"
                                                                              options:0 metrics:nil
                                                                                views:NSDictionaryOfVariableBindings(userPasswordTF)]];
    
    [self.viewRegister addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-100-[userPasswordCorrectTF]-100-|"
                                                                              options:0 metrics:nil
                                                                                views:NSDictionaryOfVariableBindings(userPasswordCorrectTF)]];
    

    

   
    
    //[userNameTF becomeFirstResponder];
    
    
    
    [userNameTF setTranslatesAutoresizingMaskIntoConstraints:NO];
    [userPasswordTF setTranslatesAutoresizingMaskIntoConstraints:NO];
    [userPasswordCorrectTF setTranslatesAutoresizingMaskIntoConstraints:NO];
    
}

-(void)updateFunc
{
//    NSLog(@"%f", self.viewMain.center.y);
    if(isMenuOpened)
    {
        self.viewMain.center = CGPointMake(380, getYPos);
    }
    else
    {
        if(!stillTouching)
        {
            if(self.viewMain.center.x > 160)
            {
                mainXPos -= 10;
                self.viewMain.center = CGPointMake(mainXPos, getYPos);
            }
            else
            {
                self.viewMain.center = CGPointMake(160, getYPos);
            }
        }
    }
    
    if(proceedNow)
    {
        if(self.viewRegister.center.y > -600)
        {
            registerYPos -= 10;
            self.viewRegister.center = CGPointMake(160, registerYPos);
        }
    }
    else
    {
        if(self.viewRegister.center.y < 280)
        {
            registerYPos += 10;
            self.viewRegister.center = CGPointMake(160, registerYPos);
        }
        else
        {
            self.viewRegister.center = CGPointMake(160, 280);
        }
    }
}




-(void) touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{
    [userNameTF resignFirstResponder];
    [userPasswordTF resignFirstResponder];
    [userPasswordCorrectTF resignFirstResponder];
    stillTouching = true;
    UITouch *myTouch =[[event allTouches] anyObject];
    CGPoint location = [myTouch locationInView:self.view];
    
    fingerYPositionStart = location.x;
    
}

-(void) touchesMoved:(NSSet*)touches withEvent:(UIEvent*)event
{
    stillTouching = true;
    UITouch *myTouch =[[event allTouches] anyObject];
    CGPoint location = [myTouch locationInView:self.view];
    
    
    fingerYPositionMoved = location.x;
    fingerYPositionDif = fingerYPositionMoved - fingerYPositionStart;
    
    if(fingerYPositionDif > 30 && !isMenuOpened)
    {
        self.viewMain.center = CGPointMake(fingerYPositionDif + 130, getYPos);
    }
    
  
    
    
    if(fingerYPositionDif >= 250)
    {
        isMenuOpened = true;
    }
    
    if(fingerYPositionDif <= -150)
    {
        isMenuOpened = false;
    }
    
}


-(void) touchesEnded:(NSSet*)touches withEvent:(UIEvent*)event
{
    fingerYPositionStart = 0;
    fingerYPositionMoved = 0;
    fingerYPositionDif = 0;
    isThisMainMenu = false;
    stillTouching = false;
}


- (IBAction)buttonRegister:(id)sender
{
    userName = userNameTF.text;
    userPassword = userPasswordTF.text;
    userPasswordCorrect = userPasswordCorrectTF.text;
    
    if([userPassword isEqualToString:userPasswordCorrect] && userNameTF.text.length > 0 && userPasswordTF.text.length > 0
       && userPasswordCorrectTF.text.length >0)
    {
        [[NSUserDefaults standardUserDefaults]setObject:userName forKey:@"userName"];
        [[NSUserDefaults standardUserDefaults]setObject:userPassword forKey:@"userPassword"];
        
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Register Complete"
                                                          message:@"Please Login Now"
                                                         delegate:nil
                                                cancelButtonTitle:@"Allright :)"
                                                otherButtonTitles:nil];
        [message show];
        [self.view endEditing:YES];
        
    }
    else
    {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Something is wrong!"
                                                          message:@"Please check your password."
                                                         delegate:nil
                                                cancelButtonTitle:@"Oh, sorry"
                                                otherButtonTitles:nil];
        [message show];
        [self.view endEditing:YES];
    }

    
}

- (IBAction)buttonLogin:(id)sender
{
    userName = userNameTF.text;
    userPassword = userPasswordTF.text;
    
    NSString *userNameTemp;
    NSString *userPasswordTemp;
    
    userNameTemp = [[NSUserDefaults standardUserDefaults]stringForKey:@"userName"];
    userPasswordTemp = [[NSUserDefaults standardUserDefaults]stringForKey:@"userPassword"];
    
    if([userName isEqualToString:userNameTemp] && [userPassword isEqualToString:userPasswordTemp]
       && userNameTF.text.length > 0 && userPasswordTF.text.length > 0)
    {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Login Succesful"
                                                          message:@"Please Proceed"
                                                         delegate:nil
                                                cancelButtonTitle:@"Enter"
                                                otherButtonTitles:nil];
        [message show];
        [self.view endEditing:YES];
        registerYPos = 284;
        proceedNow = true;
        NSLog(@"login ok");
        
    }
    else
    {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Something is Wrong"
                                                          message:@"Please Check Your Username and Password"
                                                         delegate:nil
                                                cancelButtonTitle:@"Enter"
                                                otherButtonTitles:nil];
        [message show];
        [self.view endEditing:YES];

    }
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    NSLog(@"entered");
    if(textField.tag == 10)
    {
        self.imageKey1.image = [UIImage imageNamed:@"key.png"];
    }
}



- (IBAction)buttonLogout:(id)sender
{
    proceedNow = false;
}
@end

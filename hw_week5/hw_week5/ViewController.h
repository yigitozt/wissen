//
//  ViewController.h
//  hw_week5
//
//  Created by Yigit Ozturk on 1.12.2013.
//  Copyright (c) 2013 yigitozturk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController


@property (strong, nonatomic) IBOutlet UIView *viewMain;
@property (strong, nonatomic) IBOutlet UIView *viewMenu;
@property (strong, nonatomic) IBOutlet UIView *viewRegister;


- (IBAction)buttonRegister:(id)sender;
- (IBAction)buttonLogin:(id)sender;

@property (strong, nonatomic) IBOutlet UIImageView *imageKey1;
@property (strong, nonatomic) IBOutlet UIImageView *imageKey2;
@property (strong, nonatomic) IBOutlet UIImageView *imageKey3;

- (IBAction)buttonLogout:(id)sender;

@end

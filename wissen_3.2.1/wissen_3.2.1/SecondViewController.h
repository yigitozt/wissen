//
//  SecondViewController.h
//  wissen_3.2.1
//
//  Created by Yigit Ozturk on 10.11.2013.
//  Copyright (c) 2013 wissen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondViewController : UIViewController

- (IBAction)getSwitchState:(id)sender;


@end

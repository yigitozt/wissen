//
//  SecondViewController.m
//  wissen_3.2.1
//
//  Created by Yigit Ozturk on 10.11.2013.
//  Copyright (c) 2013 wissen. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()
{
    UISwitch *switchButton;
    UISlider *slider;
}

@end

@implementation SecondViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    /////SWITCH BUTTON//////
    switchButton = [[UISwitch alloc]initWithFrame:CGRectMake(50, 100, 0, 0)];// size i sabit degismiyor
    [self.view addSubview:switchButton];
    [switchButton setOn:true];// ilk basta ne olacagi
    [switchButton addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
    switchButton.thumbTintColor = [UIColor blueColor];
    //switch degistiginde tetiklenmsi icin yazdik
    //addTarget butonun-switchin tanimlanmis oldugu classdir
    //@selector methodu icindeki isimde metod varsa bulmasini sagliyor
    
    
    
    ////SLIDER///
    
    slider = [[UISlider alloc]initWithFrame:CGRectMake(50, 200, 200, 100)];
    slider.value = 0.5f;
    slider.minimumValue = 0.0f;
    slider.maximumValue = 1.0f;
    [self.view addSubview:slider];
    
    
    [slider addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)switchChanged:(id)sender
{
    if([sender isKindOfClass:[UISlider class]])
    {
        //        slider.value = roundf(slider.value); yuvarlama
        UISlider *currentSlider = (UISlider*)sender;
        float valueGathered = roundf(currentSlider.value * 10);
        currentSlider.value = valueGathered / 10;
        
         NSLog(@"Slider value is %f",currentSlider.value);
    }
    else if([sender isKindOfClass:[UISwitch class]])
    {
        NSLog(@"Switch state is %hhd",((UISwitch*)sender).isOn);
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)getSwitchState:(id)sender
{
    
    NSLog(@"Switch state is %hhd",switchButton.isOn);
}
@end

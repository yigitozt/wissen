//
//  ThirdViewController.m
//  wissen_3.2.1
//
//  Created by Yigit Ozturk on 10.11.2013.
//  Copyright (c) 2013 wissen. All rights reserved.
//

#define LIGHTFONT(x) [UIFont fontWithName:@"Helvetica-Light" size:x]
#define COLOR(x,y,z,a) [UIColor colorWithRed:x/255.0f green:y/255.0f blue:z/255.0f alpha:a]
//renk ve font icin kisayol tanimladik

#import "ThirdViewController.h"

@interface ThirdViewController ()
{
    UILabel *mainLabel;
    UITextField *mainTextField;
}

@end

@implementation ThirdViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    mainTextField = [[UITextField alloc]initWithFrame:CGRectMake(10, 150, 150, 40)];
    [self.view addSubview:mainTextField];
    mainTextField.layer.borderWidth = 0.26f;
    mainTextField.layer.borderColor = [UIColor grayColor].CGColor;
    mainTextField.keyboardType = UIKeyboardTypeNumberPad;
    //UITextFieldDelegata ekledik h dosyasina
    NSString *tempString = mainTextField.text.uppercaseString;//stringe atama
    NSInteger tempInteger = tempString.integerValue;//int e cevirme
 
    
    
    
    NSString *homeText = @"Home Sweet Home Home sweet home";// 1
    
   
    
    
    
    mainLabel = [[UILabel alloc]init];
    mainLabel.frame = CGRectMake(10, 50, 150, 40);
    mainLabel.text = @"Home sweet home Home sweet home";
    mainLabel.backgroundColor = [UIColor greenColor];
    [self.view addSubview:mainLabel];
    mainLabel.textAlignment = NSTextAlignmentCenter;
    //mainLabel.font =[UIFont fontWithName:@"Helvetica-Light" size:14];
    mainLabel.font = LIGHTFONT(14);
    //define ile tanimladigimiz daha rahat kullanimli
    mainLabel.numberOfLines = 0;//belirsiz demek otomatik olarak yuksek kadar doldurur
    mainLabel.textColor = [UIColor redColor];
    
    
    mainLabel.attributedText = [self affiliateStringGenerator:homeText lookUpString:@"Home"];
    //tanimladigimiz metod icinde kullniyoruz, daha kolay oluyor
    
}


- (NSMutableAttributedString*)affiliateStringGenerator:(NSString*)startingText lookUpString:(NSString*)luString
{
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]initWithString:startingText];//2
    
    NSDictionary *attributesForFirstWord = @{NSFontAttributeName : LIGHTFONT(14),
                                             NSForegroundColorAttributeName : COLOR(232, 0, 27, 1),
                                             NSBackgroundColorAttributeName : [UIColor blackColor]};//3
    
    [attributedString setAttributes:attributesForFirstWord range:[startingText rangeOfString:luString]];//4
    
    return attributedString;
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

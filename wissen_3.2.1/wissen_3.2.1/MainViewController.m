//
//  MainViewController.m
//  wissen_3.2.1
//
//  Created by Yigit Ozturk on 10.11.2013.
//  Copyright (c) 2013 wissen. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()
{
    
    UIAlertView *mainAlertView;
    UIAlertView *secondAlertView;
    
}


@end

@implementation MainViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    mainAlertView = [[UIAlertView alloc]
                 initWithTitle:@"Colors"
                 message:@"Choose your color"
                 delegate:self
                 cancelButtonTitle:@"Default" // index0
                 otherButtonTitles:@"Green",@"Blue", nil];//virgul ile buttonlar eklenir sirasiyla index 1,2,
                    //delegata i nil den self yaptik

    mainAlertView.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
    UITextField *textField = [mainAlertView textFieldAtIndex:0];
    textField.placeholder = @"Kullanici Adi";
    textField.keyboardType = UIKeyboardTypeNumberPad;
    
    
    secondAlertView = [[UIAlertView alloc]
                     initWithTitle:@"Colors"
                     message:@"Choose your color"
                     delegate:self
                     cancelButtonTitle:@"Default"
                     otherButtonTitles:@"Green",@"Blue", nil];

    
    
    
}


- (IBAction)alertButton:(id)sender
{
   //alertView.alertViewStyle = UIAlertViewStyleSecureTextInput;
    
    [mainAlertView show];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
 
    if(alertView == mainAlertView)
    {
        //butun alertviewler bu delegte i cagiracagindan icinde if ile ayirt edebiliriz
    
    
    
    
    UITextField *textField = [alertView textFieldAtIndex:0];
    NSLog(@"%@", textField.text);
    
    
    switch (buttonIndex) {
        case 0:
            self.view.backgroundColor = [UIColor whiteColor];
            break;
            
        case 1:
            self.view.backgroundColor = [UIColor greenColor];
            break;
            
        case 2:
            self.view.backgroundColor = [UIColor blueColor];
            break;

            
        default:
            break;
    }
  }
}
//h dosyasina delegate i ekledigimiz icin geldi bu metod





@end

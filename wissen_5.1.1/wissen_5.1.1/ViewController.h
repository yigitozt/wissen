//
//  ViewController.h
//  wissen_5.1.1
//
//  Created by Yigit Ozturk on 30.11.2013.
//  Copyright (c) 2013 yigitozturk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *amountTF;

@property (strong, nonatomic) IBOutlet UITextField *fromTF;


@property (strong, nonatomic) IBOutlet UITextField *toTF;


@property (strong, nonatomic) IBOutlet UITextField *resultLabel;


- (IBAction)getAmounts:(id)sender;


@end

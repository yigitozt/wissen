//
//  ViewController.m
//  wissen_5.1.1
//
//  Created by Yigit Ozturk on 30.11.2013.
//  Copyright (c) 2013 yigitozturk. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
   

    
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)getAmounts:(id)sender
{
    
    
    
    NSString *urlString = [NSString stringWithFormat:@"https://currencyconverter.p.mashape.com/?from_amount=%@&from=%@&to=%@"
                        ,self.amountTF.text
                        ,self.fromTF.text
                        ,self.toTF.text];
    
    
    NSURL * url = [NSURL URLWithString:urlString];
    //url oldugu icin nsurl kullanmamiz lazim
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:url];//mutable cunku header ekleyecegiz
    [request setValue:@"UXai0hVC6827lQ9uU3j3xrHeJMz0DjK1" forHTTPHeaderField:@"X-Mashape-Authorization"];///
    //bu kisimi key girecegimizden dolayi header eklememiz gerektiginden yazdik.
    request.timeoutInterval = 20;
    
    NSOperationQueue *queue = [[NSOperationQueue alloc]init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        //servis cagrisi bittiginde bu kod blogu caliscak
        
        if([data length] > 0 && connectionError == nil)
        {
           // NSString *response = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            
            
            
            [self performSelectorOnMainThread:@selector(setLabelText:) withObject:data waitUntilDone:YES];//1//
            //ya bunu  yada alttaki methodu kullanacagiz
            
           /*
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
             // self.resultLabel.text = dict[@"to_amount"];  calismiyor cunku number donuyor
                
                self.resultLabel.text = @"aaa";
                
            });//2//
            // bunu alip main thread e atma
          */
        }
        NSLog(@"Error %@", connectionError.description);
        
    }];
    //asenkron yontem cagrinin glmesini beklerken programi bekletmeyen yontemdir. orn twitter
    //senkron yontem ise cagri gelene kadar bekletir. orn login handler

}


- (void)setLabelText:(NSData *)data
{
    NSError *error;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    NSLog(@"Data Length %@", dict);
    NSLog(@"Data Length %i", [data length]);
          
    NSString *dataString= [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    
    NSLog(@"Data Length %@", dataString);
   
    
    self.resultLabel.text = [dict[@"to_amount"] stringValue];
    
}



@end

//
//  MainViewController.m
//  wissen_2.2.1
//
//  Created by Yigit Ozturk on 3.11.2013.
//  Copyright (c) 2013 wissen. All rights reserved.
//

#import "MainViewController.h"
#import "Student.h" //
#import "math.h" //

@interface MainViewController ()

{
    NSArray * newTempArray;// buraya kendi icinde global olsun diye yaziyoruz
    //yani sadece viewdidload icine degil digerlerinde de kullanabiliriz
}

@end

@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
  
    
    
    
    //math
    

    
    double number = pow(4, 2);
    double numberOne = (abs(-5) + 5) * 2.f / 7;// bir tanesinde f olmasi yeterli float olmasi icin
    NSLog(@"%.2f", numberOne);
    
    
    
    
    
    
    
    ////////////////////////////
    NSArray *tempArray = @[@{@"name":@"Emre", @"lastname":@"Tirnovali",@"age":@30},
                           @{@"name":@"Ersun", @"lastname":@"Senturk",@"age":@30},
                           @{@"name":@"Halit", @"lastname":@"Senarslan",@"age":@30},
                           @{@"name":@"Gokhan", @"lastname":@"Arac",@"age":@30},
                           @{@"name":@"Afsin", @"lastname":@"Kocun",@"age":@30},
                           @{@"name":@"Yunus", @"lastname":@"Erol",@"age":@30},
                           ];
    
    // gecen projedeki odevin sonucu
    
    
    NSString *tempString = tempArray[4] [@"lastname"];
    NSLog(@"%@",tempString);
    //4. indexdeki lastname keyli degeri getirir
    
    
    
    
    
    Student *studentOne = [[Student alloc]init];
    studentOne.studentName = @"Emre";
    studentOne.studentName = @"Tirnovali";
    studentOne.studentId = 1024;
    studentOne.studentAge = @30;
    studentOne.city = @"istanbul";
    
    Student *studentTwo = [[Student alloc]init];
    studentTwo.studentName = @"Omer";
    studentTwo.studentName = @"Bolukbasi";
    studentTwo.studentId = 966;
    studentTwo.studentAge = @30;
    studentTwo.city = @"istanbul";
    
    Student *studentThree = [[Student alloc]init];
    studentThree.studentName = @"Bekir";
    studentThree.studentName = @"Tirnovali";
    studentThree.studentId = 1079;
    studentThree.studentAge = @29;
    studentThree.city = @"istanbul";
    
    NSArray * studentBag =@[studentOne,studentTwo,studentThree];
    
    //array icindeki nesneye erismenin yollari
    NSString *studentName = ((Student*)studentBag[2]).studentName; //1
    
    
    Student * expectedStudent = [studentBag objectAtIndex:2];// yada studentBag[2] de yazabiliriz
    NSString * studentName2 = expectedStudent.studentName;//2
    
    
    NSString *studentName3 = [(Student*)studentBag[2] studentName];//3
    
    
    
    newTempArray = [self populateArrayWithStudentObjects:tempArray];
    // bu satir ve method sayesinde yukaridaki butun satirlardaki ayni islem gerceklesiyor
    
    [self switchMechanism:15];//switchMechanism metodunu cagirdik ancak newTemp arrayi tanimladiktan sonra cagirmamiz lazim

    
    for(Student *s in newTempArray)
    {
        NSLog(@"Student Name is === %@", s.studentName);
    }
    
    
    
    
}


/////METHODS//////////



-(void)switchMechanism:(NSInteger)value
{
    
    if(newTempArray.count > 5)
    {
    
    
        switch (value) {
            case 0:
                NSLog(@"Zero value is sent");
                NSLog(@"Succeed");
                break;
            case 1:
                NSLog(@"one value is sent");
                NSLog(@"Succeed");
                break;
            
            default:
                NSLog(@"no value is sent");
                NSLog(@"Failure!");
                break;
    }
    }
}

//yeni bir method olusturduk void yerine NSMutableArray cunku disari onu vermesini istiyoruz
-(NSMutableArray*)populateArrayWithStudentObjects:(NSArray*)dataArray
{
    NSMutableArray *containerArray = [[NSMutableArray alloc]init];
    //student nesnelerini olusturduktan sonra toplayacagimiz array
    
    for(NSDictionary * s in dataArray)
    {
        Student *tempStudent = [[Student alloc]init];
        tempStudent.studentName = s[@"name"];
        tempStudent.studentLastName = s[@"lastname"];
        tempStudent.studentAge = s[@"age"];
        [containerArray addObject:tempStudent];//her dongu sonunda tempstudent i container a ekliyoruz
    }
    
    return containerArray;//container arrayi disariya vermesini istiyoruz
    
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

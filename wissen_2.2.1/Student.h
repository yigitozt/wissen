//
//  Student.h
//  wissen_2.2.1
//
//  Created by Yigit Ozturk on 3.11.2013.
//  Copyright (c) 2013 wissen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Student : NSObject
//class ise * kullaniyoruz.
@property(nonatomic,strong) NSString * studentName;
@property(nonatomic,strong) NSString * studentLastName;
@property(nonatomic,strong) NSString * city;
@property(nonatomic,strong) NSString * region;
@property(nonatomic)NSInteger studentId;
@property(nonatomic,strong) NSNumber * studentAge;






@end

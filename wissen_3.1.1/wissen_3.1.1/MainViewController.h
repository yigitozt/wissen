//
//  MainViewController.h
//  wissen_3.1.1
//
//  Created by Yigit Ozturk on 9.11.2013.
//  Copyright (c) 2013 wissen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController


@property(nonatomic,strong)IBOutlet UIView * grayRectangle;
//IBOutlet xib de gorebilecegimiz bir property yapti
//files owner bolumunde view imizi seciyoruz


@property (weak, nonatomic) IBOutlet UIView *secondView;
//control ve surukle birak yontemi

- (IBAction)pressedButtonAction:(id)sender;
//butonu ekledik action sectik

//- (IBAction)createTenButton:(id)sender;
//bunda da createTenButton methodunu cagirir ama ayni anda ikisinide yaptirmamiz mantiksiz

- (IBAction)toggleButton:(id)sender;

@end

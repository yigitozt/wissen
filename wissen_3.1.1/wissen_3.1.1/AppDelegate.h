//
//  AppDelegate.h
//  wissen_3.1.1
//
//  Created by Yigit Ozturk on 9.11.2013.
//  Copyright (c) 2013 wissen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

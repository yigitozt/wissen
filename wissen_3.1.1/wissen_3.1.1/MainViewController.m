//
//  MainViewController.m
//  wissen_3.1.1
//
//  Created by Yigit Ozturk on 9.11.2013.
//  Copyright (c) 2013 wissen. All rights reserved.
//

#import "MainViewController.h"
#import <QuartzCore/QuartzCore.h>
//quartz ile layer sinifini cagirabiliyoruz


@interface MainViewController ()

{
    bool isSquaresReady;
//    UIView *scaledView = [self.view viewWithTag:25]; neden eklemiyor ?????????
}

@end

@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    isSquaresReady = true;
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self
                                   selector:@selector(updateFunc) userInfo:nil repeats:YES];
    
    
    [self createTenSquareAtView:self.secondView];
    
    /*
    UIView *redSquare = [[UIView alloc]init];
    //UIView *redSquare = [[UIView new]; bu da ayni isi goruyo
    redSquare.frame = CGRectMake(75, 75, 30, 30);
    redSquare.backgroundColor = [UIColor redColor];
    [self.secondView addSubview:redSquare];//addSubwiev istenilen view e ekler.
    
    self.secondView.layer.cornerRadius = 8;
    self.secondView.layer.borderWidth = 4;
    self.secondView.layer.borderColor = [UIColor redColor].CGColor;
    //bunar c func oldugundan sonuna CGColor koyuyoruz
    */
    
    
    
    
    
    //yakalama teknigi 1
    UIView *rectangleView = [self.view viewWithTag:11];
    
    NSArray *rectangleViewSubviews = [rectangleView subviews];
    
    for(UIView *s in rectangleViewSubviews)
    {
      //[s removeFromSuperview];// view silme methodu
     
        s.backgroundColor = [UIColor grayColor];
        
        /*
        
        s.transform = CGAffineTransformMakeRotation(45 * M_PI/180);
        s.transform = CGAffineTransformMakeScale(0.5, 0.5);
        // bir sonraki transform objeyi orjinal halinden aliyor tekrardan. bu yuzden bir soraki transformda
        // bir onceki transform olmus halini almaliyiz s.transform gibi
        
         */
        
        
        self.grayRectangle.backgroundColor = [UIColor redColor];
        //2. metod ile yakaladigimiz viewlar
        
    }
    
    //yakalama teknigi 2////
    //h dosyasinda property ile
    
    
    
    
   // ((UIView*)rectangleViewSubviews[0]).backgroundColor = [UIColor grayColor];
    //basinda UIView cast etmemiz lazim cunku ide bunun bir UIView oldugunu bilemez.
    
    
    
    
    //NSArray *subViews = [self.view subviews];
    //NSLog(@"views :  %@", subViews);
    
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if ([touch tapCount] > 0)
    {
        UIView *scaledView = [self.view viewWithTag:25];
        CGPoint currentTouch = [touch locationInView:self.view];
        scaledView.transform = CGAffineTransformMakeScale(100/currentTouch.x, 100/currentTouch.y);
        scaledView.alpha = (15/currentTouch.x + 15/currentTouch.y);
    }
}

- (void)updateFunc
{
  
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
 
}

////create views
- (void)createTenSquareAtView:(UIView*)view
{
        for(int a = 0;a<2;a++)
        {
            for(int b = 0; b<5;b++)
            {
                UIView *sqrView = [UIView new];
                sqrView.frame = CGRectMake(60 + 35 * b, 50 + 35 * a, 30, 30);
                sqrView.backgroundColor = [UIColor blackColor];
                [view addSubview:sqrView];
            }
        }
}

- (IBAction)pressedButtonAction:(id)sender
{
    
    for(UIView *s in self.secondView.subviews)
    {
        s.backgroundColor = [UIColor greenColor];
    }
    //butona tiklandiginda yesile donusturcek
}
- (IBAction)toggleButton:(id)sender
{
    
    if(isSquaresReady)
    {
        for(UIView *s in self.secondView.subviews)
        {
            [s removeFromSuperview];
        }

        isSquaresReady = false;
    }
    else
    {
        [self createTenSquareAtView:self.secondView];
        isSquaresReady = true;
    }
}

@end

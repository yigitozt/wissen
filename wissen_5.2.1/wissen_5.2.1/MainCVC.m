//
//  MainCVC.m
//  wissen_5.2.1
//
//  Created by Yigit Ozturk on 1.12.2013.
//  Copyright (c) 2013 yigitozturk. All rights reserved.
//
#define URLSTRING @"http://api.sba.gov/geodata/city_county_links_for_state_of/ca.json"
#define URLSTRING2 @"http://itunes.apple.com/search?term=jack+johnson"

#import "MainCVC.h"
#import <AFNetworking/AFNetworking.h>
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "SecondViewController.h"
#import "Province.h"

@interface MainCVC ()
{
     NSArray *containerArray;
}

@end

@implementation MainCVC



- (void)viewDidLoad
{
    [super viewDidLoad];
	
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:URLSTRING parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
         
         //containerArray = responseObject[@"results"];
         
         containerArray = responseObject;
        
         
         NSMutableArray *mContainer = [NSMutableArray new];
         
         for(NSDictionary *s in containerArray)
         {
             Province *temp = [Province new];
             temp.county_name = s[@"county_name"];
             temp.state_abbreviation = s[@"state_abbreviation"];
             temp.full_county_name = s[@"full_county_name"];
             
             
             
             [mContainer addObject:temp];
         }
         
        
         
         
         containerArray = mContainer; // direk adresini referans eder
         //containerArray = [mContainer copy]; bunda da icerigi kopyalar
         /*
         
         NSPredicate *predicate = [NSPredicate predicateWithFormat:@"full_county_name == 'Orange County'"];
         //"full_county_name == 'Orange County' && 'asd'" gibi uzatilabilir
         
         NSArray *filteredArray = [containerArray filteredArrayUsingPredicate:predicate];
         // bu arrayda sadece full county name kismi orange county olanlar bulunacak
         //bunlari yapabilmemiz icin Province gibi elimizde nesnelerin oldigi bir class lazim
         
         containerArray = filteredArray;
         */
         
         NSArray *sortedArray;
         
         NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"full_county_name" ascending:NO];
         
         sortedArray = [containerArray sortedArrayUsingDescriptors:[NSArray arrayWithObjects:sort, nil]];
         
         containerArray = sortedArray;
         
         [self.collectionView reloadData];//bu method ile tekrardan donup ekrara yazdirmasini sagliyoruz
         //yoksa ilk basta array 0 oldugundan gelmeyebilir
         
     }
     
         failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
         
     }];
    
}



-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;//1
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return containerArray.count;//2
    //return 50;//ilk 50 datayi alalim diye girdik
}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    Province *temp = [containerArray objectAtIndex:indexPath.row];
    static NSString *CellIdentifier = @"Cell";// cell e bir identifier verip buraya yaziyoruz
   // static NSString *cellImageIdentifier = @"imageCell";
    UICollectionViewCell *cell;
  //  NSDictionary *temp =[containerArray objectAtIndex:indexPath.row];
    
    /*
    if(temp[@"artworkUrl60"]!= nil)
    {
         cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellImageIdentifier forIndexPath:indexPath];
         UIImageView *uiIV = (UIImageView*)[cell viewWithTag:12];
        
        [uiIV setImageWithURL:[NSURL URLWithString:temp[@"artworkUrl60"]]];

    }
    else
    {*/
         cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
         UILabel *name = (UILabel*)[cell viewWithTag:21];
    name.text = temp.full_county_name;
   // }
    
    
    
   
    
    
    
    return cell;
    //3
}

///// 1 2 3 metodlarini biz ekledik


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    SecondViewController *scndVC = [segue destinationViewController];
    
    NSArray *indexPathArray = [self.collectionView indexPathsForSelectedItems];
    
    NSIndexPath *indexPath = [indexPathArray objectAtIndex:0];
    
    scndVC.stateDict = [containerArray objectAtIndex:indexPath.row];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

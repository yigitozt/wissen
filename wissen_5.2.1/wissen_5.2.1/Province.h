//
//  Province.h
//  wissen_5.2.1
//
//  Created by Yigit Ozturk on 1.12.2013.
//  Copyright (c) 2013 yigitozturk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Province : NSObject

@property(nonatomic,strong)NSString *county_name;
@property(nonatomic,strong)NSString *state_abbreviation;
@property(nonatomic,strong)NSString *full_county_name;



@end

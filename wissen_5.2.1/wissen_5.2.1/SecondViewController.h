//
//  SecondViewController.h
//  wissen_5.2.1
//
//  Created by Yigit Ozturk on 1.12.2013.
//  Copyright (c) 2013 yigitozturk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondViewController : UIViewController

@property (nonatomic,strong) NSDictionary *stateDict;


@property (strong, nonatomic) IBOutlet UILabel *resultLabel;



@end

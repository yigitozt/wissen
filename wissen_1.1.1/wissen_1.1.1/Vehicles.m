//
//  Vehicles.m
//  wissen_1.1.1
//
//  Created by Yigit Ozturk on 26.10.2013.
//  Copyright (c) 2013 wissen. All rights reserved.
//

#import "Vehicles.h"
#import "AirCrafts.h"
#import "Ships.h"

#define AIR_CRAFT_NAME @"AirCraft"
#define SHIP_NAME @"Ship"

//define edip istedigimiz yerde SHIP_NAME olarak yazabiliriz.Sonucu bize Ship olarak verir yine.

@interface Vehicles()
{
    NSString * localStringVariable;
    //h dosyasi yerine burada tanimlayabiliriz
}


@end


@implementation Vehicles

//methodun basinda init olmali initialize metodu olmasi icin mecbur
-(id)initWithBrandName:(NSString*)brandName
{
    self = [super init];// bu atasi olan NSObjectteki super init cagrilmali
    _brandName = brandName; //self.brandName = brandName; de olur eger bunu yazarsak BUYUK HARF cikar
    //ama provate a atadigimiz icin suan ne yazildiysa onu verecek
    
    //self.vehicleEngineID = @5; gibi deger atayabiliriz
    
    return self;
}



- (id)init
{
    self = [super init];
    if (self) {
        //if self kismi guvenlik icin var. Bu nesne initialize edilmis mi edilmemis mi onu kontrol ediyo
        // crash olmasini engelliyor
       // self.brandName = @"Initial bran name" diyebilirdik, default olarak ,oyle baslardi
    }
    return self;
}

//init yazsak da yazmasak da NSObjectte zaten var ancak bu sekilde yazarsak init i override ederiz

// self.brandName = @"boeaing" bunun tarafindanoverride edilir
//arkada her property bi tane private ile esittir _brandName

//mesela self.brandName = @"boeaing" yazinca burasi tetiklenir.
// asdasda = self.brandName bu da get metodu

// bu bir setter
-(void)setBrandName:(NSString *)brandName
{
    _brandName = [brandName uppercaseString];
}
// bir property yazdigimizda arka planda zaten otomatik get set metodunu yapiyor ide
//_brandName class icinde private tir.
//setter da verdigimiz kosullar otomatikman bu degiskeni kullandigimizda get olur yani setterdan alinir.




-(void)printBrandName
{
    //Vehicles a da esitleseydik yine true olurdu
    if ([self isKindOfClass: [AirCrafts class]])
    {
        NSLog(@"AirCraft brand is %@ Country - %@", self.brandName, self.countryName);
        //self bu classda tanimli olan bir propertiesi ifade eder (yani this gibi)

    }
    else if ([self isKindOfClass: [Ships class]])
    {
        NSLog(@"Ship brand is %@ Country - %@", self.brandName, self.countryName);
        //self bu classda tanimli olan bir propertiesi ifade eder (yani this gibi)
    }

    
    
}

-(NSString*)getCapitalBrandName
{
    return [self.brandName uppercaseString];
    
    return self.brandName;
}


-(void)getVehicleSpecifications:(NSInteger)tonage colorName:(NSString *)color
{
    NSLog(@"The %@  brand vehicle specification: \n tonage is %i \n country name is %@ and color is %@ \n ---------",
          self.brandName,
          tonage,
          self.countryName,
          color);
    
    // [self printClassName];
    // ayni sinif icinde tanimladigimiz metodu burada self ile cagirabiliriz
    //ancak baska siniflardan goremeyiz
}

// fonksiyona parametre ekledikce aralara bosluk birakilir tonagedan sonra gelen colorName gibi

//strinde alt satira ineceksek eger
// @"asdasdasdasdasdasda"  altsatirda yine @"asdasdasdasdasd" diye devam edilir


-(void)printClassName
{
    NSLog(@"class name is %@", self.class);
    //bu satir planes de cagrilir, atasinda yaziyoruz ama torunundan cagiriliyor. Polymorphisim
}


@end








//
//  openViewController.m
//  wissen_1.1.1
//
//  Created by Yigit Ozturk on 26.10.2013.
//  Copyright (c) 2013 wissen. All rights reserved.
//

#import "openViewController.h"
#import "Planes.h"
#import "Ships.h"

@interface openViewController ()

@end

@implementation openViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //burasi acilmadan cagriliyor
    //class i initialize ediyoruz
    //artik bu class hayata gecti
    
    Planes* planeZero = [[Planes alloc]initWithBrandName:@"boeing"];
   
    planeZero.countryName = @"United States";
    //NSLog(@"Capital of Brand is = %@", [planeZero getCapitalBrandName]);
    
    [planeZero printBrandName];
    
    //[planeZero getVehicleSpecifications:5000 colorName:@"red"];
    //[planeZero printClassName];
    
    Ships* shipZero = [[Ships alloc]init];
    shipZero.brandName = @"SunSeeker";
    shipZero.countryName = @"Germany";
    
    [shipZero printBrandName];
    
    
    

    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

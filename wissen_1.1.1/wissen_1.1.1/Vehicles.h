//
//  Vehicles.h
//  wissen_1.1.1
//
//  Created by Yigit Ozturk on 26.10.2013.
//  Copyright (c) 2013 wissen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Vehicles : NSObject


/*
{
    NSString * localStringVariable;
    //buna diger hicbir class erisemez ama kendi m dosyasi erisebilir
}
*/




//burada tanimlananlar disaridan erisilebilir olacaktir.
//1 classin sadece1 tane superclassi olur

//property tanimlayacagiz


@property(nonatomic,strong)NSString * brandName;
//nonatomic ve strong yada weak hep yazilir, istenirse readandwrite yada onlyread de eklenebilir,setter ve getter i etkiler
@property(nonatomic,strong)NSString * countryName;

//@property(nonatomic,strong)NSString * color;

@property (nonatomic)NSInteger vehicleWeight;// %i
@property (nonatomic)float vehicleTotalCost;// %f
@property (nonatomic)double vehicleHeight;
@property (nonatomic)NSUInteger seatCount;//sadece pozitif
@property (nonatomic)BOOL isBigEnough;//boolda da yildiz yok cunku 0 1



//sayilarda strong yada weak yok

@property (nonatomic,strong)NSNumber * vehicleEngineID; // NSNumber bir class



-(void)printBrandName;
//bunu h da tanimlayip m de kullaniyoruz
-(NSString*)getCapitalBrandName;

//metoda parametre veriliyor
-(void)getVehicleSpecifications:(NSInteger)tonage colorName:(NSString*)color;

//bunda * yok cunku yildiz adresi isaret ediyor. String icin bir adres tutuluyor, data da o adrede durur.
//pointer... sayilarda durum farkli

-(void)printClassName;

-(id)initWithBrandName:(NSString*)brandName;



@end

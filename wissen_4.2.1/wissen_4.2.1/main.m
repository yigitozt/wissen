//
//  main.m
//  wissen_4.2.1
//
//  Created by Yigit Ozturk on 24.11.2013.
//  Copyright (c) 2013 yigitozturk. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

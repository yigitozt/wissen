//
//  SecondViewController.m
//  wissen_4.2.1
//
//  Created by Yigit Ozturk on 24.11.2013.
//  Copyright (c) 2013 yigitozturk. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()

@end

@implementation SecondViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.nameLabel.text = self.individualPerson.firstName;
    self.lastNameLabel.text = self.individualPerson.lastName;
    self.ageLabel.text = self.individualPerson.age.stringValue;
    
    if(self.individualPerson.department.integerValue == 0)
    {
        self.departmentLabel.text = @"Accounting";
    }
    else
    {
        self.departmentLabel.text = @"Sales";
    }
    
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

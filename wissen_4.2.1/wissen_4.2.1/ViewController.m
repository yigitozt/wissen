//
//  ViewController.m
//  wissen_4.2.1
//
//  Created by Yigit Ozturk on 24.11.2013.
//  Copyright (c) 2013 yigitozturk. All rights reserved.
//

#import "ViewController.h"
#import "Person.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    NSArray *personArray = [self parseFromLocalFile];
    
  //  NSString *str = ((Person*)personArray[0]).firstName;//1
    
    Person *temp = personArray[0];//2
    
    NSLog(@"First Name is %@", temp.firstName);
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(NSMutableArray*)parseFromLocalFile //1
{
    NSMutableArray *container;//2
    
    
    NSError *err = nil;
    NSString *dataPath = [[NSBundle mainBundle] pathForResource:@"person" ofType:@"json"];
    //nsbundle mainbundle yandaki dosyalari okuyabilmemizi sagliyor
    
    
    
    NSArray *transaction = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfFile:dataPath] options:kNilOptions error:&err];
    //bu class jsondakidatayi almak icin kullaniliyor
    //basina array degil dictionary yaiyoruz cunku json umuz bir dict
    //transaction komple json icerigi oldu
    
    container = [NSMutableArray new];//initialize etmez isek icerisine nesne ekleyemeyiz
    
    /*
    for(NSDictionary *temp in transaction)
    {
        if([temp[@"department"]intValue] == 0)
        {
             NSLog(@"%@ %@", temp[@"firstName"], temp[@"lastName"]);
        }
       
    }
     */
    
    for(NSDictionary *temp in transaction)
    {
        Person *pTemp = [Person new];
        pTemp.firstName = temp[@"firstName"];
        pTemp.lastName = temp[@"lastName"];
        pTemp.age = temp[@"age"];
        pTemp.department = temp[@"department"];
        
        [container addObject:pTemp];
    }
    
    
    return container;//3
}

@end

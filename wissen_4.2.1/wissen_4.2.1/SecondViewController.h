//
//  SecondViewController.h
//  wissen_4.2.1
//
//  Created by Yigit Ozturk on 24.11.2013.
//  Copyright (c) 2013 yigitozturk. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "Person.h"

@interface SecondViewController : UIViewController

@property(nonatomic, strong)Person *individualPerson;


@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *lastNameLabel;

@property (strong, nonatomic) IBOutlet UILabel *ageLabel;
@property (strong, nonatomic) IBOutlet UILabel *departmentLabel;


@end

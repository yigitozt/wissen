//
//  Person.h
//  wissen_4.2.1
//
//  Created by Yigit Ozturk on 24.11.2013.
//  Copyright (c) 2013 yigitozturk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject

/*
 
 "firstName": "Jim",
 "lastName": "Galley",
 "age": 41,
 "department":0
 
 
 */



@property(nonatomic,strong)NSString *firstName;
@property(nonatomic,strong)NSString *lastName;
@property(nonatomic,strong)NSNumber *age;
@property(nonatomic,strong)NSString *department;




@end

//
//  AppDelegate.h
//  wissen_4.2.1
//
//  Created by Yigit Ozturk on 24.11.2013.
//  Copyright (c) 2013 yigitozturk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

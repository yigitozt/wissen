//
//  MyTableViewController.m
//  wissen_4.2.1
//
//  Created by Yigit Ozturk on 24.11.2013.
//  Copyright (c) 2013 yigitozturk. All rights reserved.
//

#import "MyTableViewController.h"
#import "Person.h"
#import "SecondViewController.h"

@interface MyTableViewController ()
{
     NSArray *generalData;
}

@end

@implementation MyTableViewController



- (void)viewDidLoad
{
    [super viewDidLoad];

    generalData =[self parseFromLocalFile];
   
    
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*
    if(indexPath.row > 3)
    {
        return 100;
    }
    */
    return 100.f;//satirin yuksekligini hesaplama ayarlama
    //ornegun contente gore yukseklikler ayarlanabilir.
}




#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;//section sayisi
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    //return 5;//row sayisi
    //bunun yerine
    return generalData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"simpleCell";//buraya yazdigimiz identifieri giriyoruz
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    
    Person *temp = generalData[indexPath.row]; ///////
    
    
    //cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", temp.firstName, temp.lastName];
    
    
    //tableviewin ici burada doluyor
    
    
    UILabel *labelAge = (UILabel*)[cell viewWithTag:101];
    UILabel *labelDepartment = (UILabel*)[cell viewWithTag:102];
    UILabel *labelName = (UILabel*)[cell viewWithTag:103];
    UILabel *labelLastName = (UILabel*)[cell viewWithTag:104];
    
    labelName.text = temp.firstName;
    labelLastName.text = temp.lastName;
    
    
    //labellari yakaladik
    
    labelAge.text = [temp.age stringValue];
    
    if(temp.department.intValue == 0)
    {
        labelDepartment.text = @"Acconting";
    }
    else
    {
        labelDepartment.text = @"Sales";
    }
    
    
    return cell;
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *index = self.tableView.indexPathForSelectedRow;//hangi row oldugunun indexini alabiliyoruz
    NSLog(@"%@", ((Person*)generalData[index.row]).firstName);
    
    SecondViewController * destinationVC = [segue destinationViewController];
    destinationVC.individualPerson = generalData[index.row];
    
    
}
 
 




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    NSLog(@"%i %i", indexPath.section, indexPath.row);// section ve row u buradan alabiliriz
    //tikladikca burasi calisir
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */


//ger json dict olsaydi dict dondurmemiz gerekirdi
-(NSMutableArray*)parseFromLocalFile //1  //array donduruyor cunku jsonumuz bir aray
{
    NSMutableArray *container;//2
    
    
    NSError *err = nil;
    NSString *dataPath = [[NSBundle mainBundle] pathForResource:@"person" ofType:@"json"];
    //nsbundle mainbundle yandaki dosyalari okuyabilmemizi sagliyor
    
    
    
    NSArray *transaction = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfFile:dataPath] options:kNilOptions error:&err];
    //bu class jsondakidatayi almak icin kullaniliyor
    //basina array degil dictionary yaiyoruz cunku json umuz bir dict
    //transaction komple json icerigi oldu
    
    container = [NSMutableArray new];//initialize etmez isek icerisine nesne ekleyemeyiz
    
    /*
     for(NSDictionary *temp in transaction)
     {
     if([temp[@"department"]intValue] == 0)
     {
     NSLog(@"%@ %@", temp[@"firstName"], temp[@"lastName"]);
     }
     
     }
     */
    
    for(NSDictionary *temp in transaction)
    {
        Person *pTemp = [Person new];
        pTemp.firstName = temp[@"firstName"];
        pTemp.lastName = temp[@"lastName"];
        pTemp.age = temp[@"age"];
        pTemp.department = temp[@"department"];
        
        [container addObject:pTemp];
    }
    
    
    return container;//3
}




@end

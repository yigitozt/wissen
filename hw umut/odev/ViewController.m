//
//  ViewController.m
//  odev
//
//  Created by Wissen on 05/12/13.
//  Copyright (c) 2013 Wissen. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UITextFieldDelegate>
{
    UITextField *textField1;
    UITextField *textField2;
    UITextField *textField3;
    UIImageView *key1;
    UIImageView *key2;
    UIImageView *key3;
}

@end




@implementation ViewController


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for(UITouch *touch in touches)
    {
        if([touch.view isEqual:self.view])
        {
            [self.view endEditing:YES];
        }
    }
}
//klavye cikinca yukari kaysin


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
	
    key1 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"register_field_password"]];
    key2 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"register_field_password"]];
    key3 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"register_field_password"]];
    key1.alpha = 0.2;
    key2.alpha = 0.2;
    key3.alpha = 0.2;
    
    [self.view addSubview:key1];
    [self.view addSubview:key2];
    [self.view addSubview:key3];

    
    
    UIView *line1 = [UIView new];
    line1.backgroundColor = [UIColor grayColor];
 
    UIView *line2 = [UIView new];
    line2.backgroundColor = [UIColor grayColor];
    
    UIView *line3 = [UIView new];
    line3.backgroundColor = [UIColor grayColor];
   
    UIView *line4 = [UIView new];
    line4.backgroundColor = [UIColor grayColor];
    
    UIView *line5 = [UIView new];
    line5.backgroundColor = [UIColor grayColor];
 
    
    //-----------------------------
    [self.view addSubview:line1];
    [self.view addSubview:line2];
    [self.view addSubview:line3];
    [self.view addSubview:line4];
    [self.view addSubview:line5];
    
    textField1 = [[UITextField alloc] init];
    textField1.borderStyle = UITextBorderStyleNone;
    textField1.placeholder = @"Mevcut Sifre";
    textField1.secureTextEntry = YES;
    textField1.delegate = self;
    textField2 = [[UITextField alloc] init];
    textField2.borderStyle = UITextBorderStyleNone;
    textField2.placeholder = @"Yeni Sifre";
    textField2.secureTextEntry = YES;
    textField2.delegate = self;
    textField3 = [[UITextField alloc] init];
    textField3.borderStyle = UITextBorderStyleNone;
    textField3.placeholder = @"Yeni Sifre Tekrar";
    textField3.secureTextEntry = YES;
    textField3.delegate = self;
    
    [self.view addSubview:textField1];
    [self.view addSubview:textField2];
    [self.view addSubview:textField3];
    
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-100-[line1(1)]-3-[key1(30)]-3-[line2(1)]-40-[line3(1)]-3-[key2(30)]-3-[line4(1)]-3-[key3(30)]-3-[line5(1)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line1,line2,line3,line4,line5,key1,key2,key3)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-104-[textField1(30)]-48-[textField2(30)]-7-[textField3(30)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(textField1,textField2,textField3)]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[line1(280)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line1)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[line2(280)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line2)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[line3(280)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line3)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[line4(280)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line4)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[line5(280)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line5)]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[key1(30)]-[textField1(150)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(key1,textField1)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[key2(30)]-[textField2(150)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(key2,textField2)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[key3(30)]-[textField3(150)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(key3,textField3)]];
    
    line1.translatesAutoresizingMaskIntoConstraints = NO;
    line2.translatesAutoresizingMaskIntoConstraints = NO;
    line3.translatesAutoresizingMaskIntoConstraints = NO;
    line4.translatesAutoresizingMaskIntoConstraints = NO;
    line5.translatesAutoresizingMaskIntoConstraints = NO;
    key1.translatesAutoresizingMaskIntoConstraints = NO;
    key2.translatesAutoresizingMaskIntoConstraints = NO;
    key3.translatesAutoresizingMaskIntoConstraints = NO;
    
    
    for(UIView * view in self.view.subviews)
    {
        view.translatesAutoresizingMaskIntoConstraints = NO;
    }
    

    self.navigationController.navigationBar.barTintColor = [UIColor redColor];

}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (!textField1.isEditing)
    {
        [UIView beginAnimations:NULL context:NULL];
        [UIView setAnimationDuration:1.0];
        
        key1.alpha = 0.2;
        [UIView commitAnimations];
    }
    if (!textField2.isEditing)
    {
        [UIView beginAnimations:NULL context:NULL];
        [UIView setAnimationDuration:1.0];
        
        key2.alpha = 0.2;
        [UIView commitAnimations];
    }
    if (!textField3.isEditing)
    {
        [UIView beginAnimations:NULL context:NULL];
        [UIView setAnimationDuration:1.0];
        
        key3.alpha = 0.2;
        [UIView commitAnimations];
    }

}

-(void)textFieldDidBeginEditing:(UITextField*)textField
{
    if (textField1.isEditing)
    {
        [UIView beginAnimations:NULL context:NULL];
        [UIView setAnimationDuration:1.0];
        
        key1.alpha = 1;
        [UIView commitAnimations];
    }
    if (textField2.isEditing)
    {
        [UIView beginAnimations:NULL context:NULL];
        [UIView setAnimationDuration:1.0];
        
        key2.alpha = 1;
        [UIView commitAnimations];
    }
    if (textField3.isEditing)
    {
        [UIView beginAnimations:NULL context:NULL];
        [UIView setAnimationDuration:1.0];
        
        key3.alpha = 1;
        [UIView commitAnimations];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSUInteger newLength = [textField.text length] + [string length];
    
    
    
    return (newLength > 6) ? NO : YES;
}







@end

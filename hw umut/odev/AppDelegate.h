//
//  AppDelegate.h
//  odev
//
//  Created by Wissen on 05/12/13.
//  Copyright (c) 2013 Wissen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
